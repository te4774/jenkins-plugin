#
#  Copyright (c) 2020 - 2022 Henix, henix.fr
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

FROM jenkins/jenkins:2.249.2-lts-jdk11

# First level plugin versions (plugins we want to install)
ARG OTF_PLUGIN_FOR_JENKINS_VERSION
ARG PIPELINE_VERSION=2.6
ARG GIT_VERSION=4.6.0
ARG PIPELINE_MODEL_VERSION=1.9.2
ARG CFG_FILE_PROVIDER_VERSION=3.8.0
# Dependencies from above that we need to version control for them to load
ARG PIPELINE_BASIC_STEPS_VERSION=2.20
ARG PIPELINE_MULTIBRANCH_VERSION=2.24
ARG PIPELINE_API_VERSION=1136.v7f5f1759dc16
ARG PIPELINE_SHARED_GROOVY_LIBRARIES=2.19
ARG DURABLE_TASK_PLUGIN=1.37
ARG MILESTONE_STEP_VERSION=1.3.2
ARG TOKEN_MACRO_VERSION=2.13
ARG JUNIT_VERSION=1.48
ARG ECHARTS_API_VERSION=4.9.0-3
ARG JQUERY3_API_VERSION=3.6.0-1
ARG FONT_AWESOME_API=5.15.2-2
ARG PLUGIN_UTIL_API_VERSION=2.3.0
ARG GIT_CLIENT_VERSION=3.6.0
ARG BRANCH_API_VERSION=2.6.2
ARG CHECKS_API_VERSION=1.5.0
ARG PLAIN_CREDENTIALS_VERSION=1.7
ARG FOLDERS_VERSION=6.17

USER jenkins

RUN /usr/local/bin/install-plugins.sh \
    workflow-aggregator:${PIPELINE_VERSION} workflow-basic-steps:${PIPELINE_BASIC_STEPS_VERSION} workflow-multibranch:${PIPELINE_MULTIBRANCH_VERSION} \
    workflow-api:${PIPELINE_API_VERSION} pipeline-milestone-step:${MILESTONE_STEP_VERSION} durable-task:${DURABLE_TASK_PLUGIN} \
    workflow-cps-global-lib:${PIPELINE_SHARED_GROOVY_LIBRARIES} token-macro:${TOKEN_MACRO_VERSION} \
    checks-api:${CHECKS_API_VERSION} jquery3-api:${JQUERY3_API_VERSION} font-awesome-api:${FONT_AWESOME_API} plugin-util-api:${PLUGIN_UTIL_API_VERSION} \
    junit:${JUNIT_VERSION} echarts-api:${ECHARTS_API_VERSION} \
    pipeline-model-definition:${PIPELINE_MODEL_VERSION} \
    git:${GIT_VERSION} git-client:${GIT_CLIENT_VERSION} branch-api:${BRANCH_API_VERSION} \
    plain-credentials:${PLAIN_CREDENTIALS_VERSION} cloudbees-folder:${FOLDERS_VERSION} \
    credentials credentials-binding config-file-provider:${CFG_FILE_PROVIDER_VERSION} && mkdir -p /var/jenkins_home/plugins/

COPY --chown=jenkins:jenkins opentestfactory-orchestrator-${OTF_PLUGIN_FOR_JENKINS_VERSION}.hpi /var/jenkins_home/plugins/opentestfactory-orchestrator.hpi
COPY --chown=jenkins:jenkins *.xml /var/jenkins_home/
# Seeding admin users - the old way
COPY --chown=jenkins:jenkins users /var/jenkins_home/users
# From now on, test data
COPY --chown=jenkins:jenkins jobs /var/jenkins_home/jobs
COPY --chown=jenkins:jenkins org.*.xml /var/jenkins_home/
COPY --chown=jenkins:jenkins credentials.xml /var/jenkins_home/

COPY instance_tune.groovy /usr/share/jenkins/ref/init.groovy.d/
