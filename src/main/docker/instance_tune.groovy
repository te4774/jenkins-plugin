/**
 *  Copyright (c) 2020 - 2022 Henix, henix.fr
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import jenkins.model.JenkinsLocationConfiguration;

String instanceName=System.getenv("INSTANCE_NAME");
String licenseType=System.getenv("LICENSE_TYPE");
System.out.println("Configuration of jenkins in "+licenseType+" instance "+instanceName);

JenkinsLocationConfiguration locationCfg=JenkinsLocationConfiguration.get();

String url=locationCfg.getUrl();

String configuredUrl=url.replace("demo-tf2-tm",instanceName+"-tf2-tm-"+licenseType);

locationCfg.setUrl(configuredUrl)