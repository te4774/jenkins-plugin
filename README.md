<!--

     Copyright (c) 2020 - 2022 Henix, henix.fr

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.

-->
# opentf-jenkins-plugin

This package is part of the OpenTestFactory initiative.

## Overview

This Jenkins plugin facilitates the sending of a PEaC to the OpenTestFactory Orchestrator from a Jenkins pipeline.

## Getting started

### Installation of OpenTestFactory Jenkins Plugin

To install it, you have to submit the *opentestfactory-orchestrator.hpi* file in the *Upload Plugin* area accessible 
by the *Advanced* tab of the *Plugin Manager* in *Jenkins* configuration:

![jenkins-plugin-upload](docs/img/opentestfactory-orchestrator-plugin-jenkins-upload.png){class="center"}

This plugin is compatible with version *2.164.1* or higher of *Jenkins*.

### Configuring OpenTestFactory Orchestrator in Jenkins

To access the configuration of the **OpenTestFactory Orchestrator**, you first need to go the *Configure System* page accessible 
in the *System Configuration* space of *Jenkins*, through the *Manage Jenkins* tab:

![jenkins-system-configuration](docs/img/jenkins-system-configuration.png){class="center"}

A block named *OpenTestFactoryOrchestrator servers* will then be available:

![jenkins-orchestrator-server](docs/img/jenkins-opentestfactory-orchestrator-server.png){class="center"}

* ``Server id``: This ID is automatically generated and can't be modified. It is not used by the user.

* ``Server name``: This name is defined by the user. It is the one that will be mentioned in the pipeline script of the workflow to be executed.

* ``Receptionist endpoint URL``: The address of the *receptionist* micro-service of the orchestrator, with its port as defined at the launch of 
  the orchestrator. Please refer to the **OpenTestFactory Orchestrator** documentation for further details.

* ``Workflow Status endpoint URL``: The address of the *observer* micro-service of the orchestrator, with its port as defined at the launch of 
  the orchestrator. Please refer to the **OpenTestFactory Orchestrator** documentation for further details.

* ``Credential``: *Secret text* type *Jenkins* credential containing a *JWT Token* allowing authentication to the orchestrator.
  Please refer to the <a href="https://opentestfactory.org/installation.html#trusted-keys" target="_blank">**OpenTestFactory Orchestrator** documentation</a> for further details on secure access to the orchestrator.

* ``Workflow Status poll interval``: This parameter sets the interval between each update of the workflow status.

* ``Workflow creation timeout``: Timeout used to wait for the workflow status to be available on the *observer* after reception by the *receptionist*

### Call to the OpenTestFactory Orchestrator from a Jenkins pipeline

Once there is at least one **OpenTestFactory Orchestrator** configured in *Jenkins*, it is possible to call the **OpenTestFactory Orchestrator** 
from a *pipeline* type job in *Jenkins* thanks to a dedicated pipeline method.

Below is an example of a simple pipeline using the calling method to the orchestrator:

``` groovy
node {
   stage 'Stage 1 : sanity check'
   echo 'OK pipelines work in the test instance'
   stage 'Stage 2 : steps check'
   configFileProvider([configFile(fileId: 'a9b08830-42de-44f0-87ef-45a6db7fff0c',targetLocation: 'workflow.json')]) {
        def workflow_id = runOTFWorkflow(
        workflowPathName:'workflow.json',
        workflowTimeout: '200S',
        serverName:'defaultServer'
        )
   }
}
```

The *runOTFWorkflow* method allows the transmission of a PEaC to the orchestrator for an execution.

It uses 3 parameters:

* ``workflowPathName``: The path to the file containing the PEaC. In the present case, the file is injected through the *Config File Provider* 
  plugin, but it is also possible to get it through other means (retrieval from a SCM, on the fly generation in a file, ...).

* ``workflowTimeout``: Timeout on workflow execution. This timeout will trigger if workflow execution takes longer than expected, for any reason. 
  This aims to end stalled workflows (for example due to unreachable environments or unsupported action calls). 
  It is to be adapted depending on the expected duration of the execution of the various tests in the PEaC.

* ``serverName``: Name of the **OpenTestFactory Orchestrator** server to use. This name is defined in the *OpenTestFactory Orchestrator servers* 
  space of the *Jenkins* configuration.
  
## License

Copyright 2021 Henix, henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
